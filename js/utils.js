function getURLParam(key) {
    // 获取url，并将URI进行解码
    var url = decodeURI(window.location.toString());
    var array = url.split("?");
    if (array.length > 1) {
        var params = array[1].split("&");
        for (var i = 0; i < params.length; i++) {
            var kv = params[i].split("=");
            if (kv[0] == key) {
                return kv[1];
            }
        }
    }
    return null;
}

// 批量添加参数去指定页面
function batchAddParamToPage(baseUrl, arr) {
    // 判断此时是否有随机数拼接
    var index = baseUrl.indexOf("?");
    if ((index > 0 && baseUrl.charAt(index + 1) != 'm') || index < 0) {
        var randomNum = Math.random();
        baseUrl = baseUrl + "?m=" + randomNum;
    }
    var len = arr.length;
    for (var i = 0; i < len; i++) {
        var key = arr[i].key;
        var value = arr[i].value;
        baseUrl = baseUrl + "&" + key + "=" + value;
    }
    return baseUrl;
}

// 添加单个参数去指定页面
function addParamToPage(baseUrl, paramKey, paramValue) {
    // 判断此时是否有随机数拼接
    var index = baseUrl.indexOf("?");
    if ((index > 0 && baseUrl.charAt(index + 1) != 'm') || index < 0) {
        var randomNum = Math.random();
        baseUrl = baseUrl + "?m=" + randomNum;
    }
    baseUrl = baseUrl + "&" + paramKey + "=" + paramValue;
    return baseUrl;
}