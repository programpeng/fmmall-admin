// 定义键值对之间的分隔符
var operator = "=";

// 根据键获取cookie的值
function getCookieValue(key) {
    // 获取cookie字符串对象
    var cookieStr = document.cookie;
    // 按'; '切割cookieStr字符串
    var array = cookieStr.split("; ");
    for (var i = 0; i < array.length; i++) {
        // 按'='切割键值对
        var kvStr = array[i].split(operator);
        var k = kvStr[0];
        var v = kvStr[1];
        if (k === key) {
            return v;
        }
    }
    return null;
}

// 将键值对保存到cookie中
function setCookieValue(key, value) {
    document.cookie = key + operator + value;
}

// 根据key删除指定cookie中保存的值
function removeCookieValue(key) {
    // 获取cookie字符串对象
    var cookieStr = document.cookie;
    var newCookieStr = "";
    // 按'; '切割cookieStr字符串
    var array = cookieStr.split("; ");
    for (var i = 0; i < array.length; i++) {
        // 按'='切割键值对
        var kvStr = array[i].split(operator);
        var k = kvStr[0];
        var v = kvStr[1];
        if (k === key) {
            continue;
        }
        newCookieStr += (k + "=" + v + "; ");
    }
    // 去掉最后一个;
    newCookieStr = newCookieStr.substring(0, newCookieStr.length - 2);
    alert(newCookieStr);
    document.cookie = newCookieStr;
}
